<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;
use App\SummaryOfOrganization\SummaryOfOrganization;


$obj = new SummaryOfOrganization();
$obj->setData($_GET);

$oneData  =  $obj->view();


?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Summary Of Organization | Single View</title>



    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>




</head>
<body>

<div class="container">


<?php

         echo "
             <h1> Single SummaryOfOrganization Information </h1>
               
             <table class='table table-bordered table-striped'>
             
                    <tr>                   
                        <td>  <b>ID</b>  </td>                
                        <td>  <b>$oneData->id</b>  </td>                
                      
                    </tr>
        
                     <tr>                   
                        <td>  <b>Organization Name</b>  </td>                
                        <td>  <b>$oneData->organization_name</b>  </td>                
                      
                    </tr>
                         
                     <tr>                   
                        <td>  <b>Summary</b>  </td>                
                        <td>  <b>$oneData->summary</b>  </td>                
                      
                    </tr>
                
             
             </table>

         ";


?>

</div>

</body>
</html>
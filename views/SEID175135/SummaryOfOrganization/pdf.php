<?php
include_once ('../../../vendor/autoload.php');
use App\SummaryOfOrganization\SummaryOfOrganization;

$obj= new SummaryOfOrganization();
 $allData=$obj->index();
 //var_dump($allData);
$trs="";
$sl=0;

    foreach($allData as $oneData) {
        $id =  $oneData->id;
        $name = $oneData->organization_name;
        $summary =$oneData->summary;

        $sl++;
        $trs .= "<tr>";
        $trs .= "<td width='50'> $sl</td>";
        $trs .= "<td width='50'> $id </td>";
        $trs .= "<td width='100'> $name </td>";
        $trs .= "<td width='400'> $summary </td>";

        $trs .= "</tr>";
    }

$html= <<<BITM
<div class="table-responsive">
            <table class="table" border="1">
                <thead>
                <tr>
                    <th align='center'>Serial</th>
                    <th align='center' >ID</th>
                    <th align='center' >Organation Name</th>
                    <th align='center' >Summary</th>

              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>


BITM;


// Require composer autoload
require_once ('../../../vendor/mpdf/mpdf/src/Mpdf.php');
//Create an instance of the class:

$mpdf = new \Mpdf\Mpdf();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('organization_list.pdf', 'D');
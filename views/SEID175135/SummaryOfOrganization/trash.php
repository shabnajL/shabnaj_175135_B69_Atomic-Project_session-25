<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;

use App\Utility\Utility;

use App\SummaryOfOrganization\SummaryOfOrganization;


$obj = new SummaryOfOrganization();
$obj->setData($_GET);

$obj->trash();

Utility::redirect("index.php");


?>

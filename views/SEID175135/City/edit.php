<?php
require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;
use App\City\City;

$obj = new City();
$obj->setData($_GET);
$oneData = $obj->view();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>City | Edit</title>

    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <script src="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.css"></script>


</head>
<body>

<div id="message" class="bg-primary text-center" > <?php echo Message::message() ?> </div>

<div class="container" style="margin-top: 100px">

    <h1 style="text-align: center"> City - Edit Form </h1>

    <div class="col-md-2"> </div>


    <div class="col-md-8" style="margin-top: 50px; margin-bottom: 50px">


        <form action="update.php" method="post">




            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="Name" value="<?php echo $oneData->name ?>">
            </div>



            <div class="form-group">
                <label for="city">City</label>
                <select class="form-control" id="city" name="City" required>
                    <option <?php if ($oneData->city == "Chittagong") echo "selected"; ?> value="Chittagong">Chittagong</option>
                    <option <?php if ($oneData->city == "Dhaka") echo "selected"; ?> value="Dhaka">Dhaka</option>
                    <option <?php if ($oneData->city == "Rajshahi") echo "selected"; ?> value="Rajshahi">Rajshahi</option>
                    <option <?php if ($oneData->city == "Comilla") echo "selected"; ?> value="Comilla">Comilla</option>
                    <option <?php if ($oneData->city == "Barishal") echo "selected"; ?> value="Barishal">Barishal</option>
                    <option <?php if ($oneData->city == "Sylhet") echo "selected"; ?> value="Sylhet">Sylhet</option>
                    <option <?php if ($oneData->city == "Dinajpur") echo "selected"; ?> value="Dinajpur">Dinajpur</option>
                    <option <?php if ($oneData->city == "Khulna") echo "selected"; ?> value="Khulna">Khulna</option>
                </select>
            </div>


            <div class="form-group">
                <input type="hidden" name="id" value="<?php echo $oneData->id ?>"
            </div>


            <button type="submit" class="btn btn-success">Update</button>




        </form>

    </div>


    <div class="col-md-2" > </div>


</div>

<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>

<script>


    $(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    });




</script>


</body>
</html>